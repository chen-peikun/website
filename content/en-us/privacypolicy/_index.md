+++
title = "Privacy Policy"
+++

This website (https://openlookeng.io) is operated by openLooKeng Community (hereinafter referred to as "we" or the "Community"). We understand how important your privacy is to you and fully respect it. Please carefully read and understand this Privacy Statement ("Statement") before you agree to submit your personal data to us. If you have any further questions about how we process your personal data, please feel free to contact us.

### 1. We will collect the following personal data from you

* When you sign our Contributor Agreement (CLA), we will collect the following personal data:
   + your name or account;
   + your address;
   + your email address;
   + your phone number;
   + your fax number;
   + your ID in Gitee.

* When you use our mail subscription service, we will collect the following personal data:
   + your email address.

* When you use our Tryme service, subject to your authorization, we will collect the following personal data from third parties: 
   + your username;
   + your name;
   + your user ID;
   + your email addresses.

* To keep track of the running status of the Community so as to provide better service for you, we will collect your information when you access the website, including but not limited to your IP address, browser type, used language, access date and time, software and hardware features, web page you accessed, device model, device identity code, operating system, resolution, telecom carrier, and download clicks.
### 2. How will we process your personal data

We will not use your personal data for any other purpose except under the following circumstances:

*  match your ID in Gitee to confirm that you have signed the CLA before submitting codes;
*  provide you with mail subscription service so you can participate in discussions in the community;
*  measure use of, analyze performance of, improve and develop this Community;
*  inform you of important updates about this Community;
*  allow you to directly log in and use our products and/or services through a third-party account.

### 3. How will we protect your personal data

We attach great importance to the protection of personal data. We take appropriate physical, administrative and technical safeguards to protect your personal data from unauthorized access, disclosure, use, modification, damage or loss.

### 4. How your personal data is transferred across borders

Different countries and regions may have different data protection laws. In such cases, we will take measures to handle personal data collected according to applicable laws and regulations.

### 5. How to access or control your personal data

In accordance with applicable laws and regulations, you may have the right to: 

*  access any personal data we hold about you; 
*  request us to update or correct your personal data; 
*  request us to delete your personal data; 
*  refuse or restrict our use of your personal data; 
*  obtain a copy of your personal data. 

If you need our assistance in exercising these rights, please contact us via the contact details at the end.

### 6. How to update this Privacy Policy

We may update or modify this privacy policy from time to time as our services or data processing changes. If we update this privacy policy, we will publish the latest version of the privacy policy on this website, which will come into effect immediately after being published. We recommend you to check this privacy policy regularly for any changes being made. If we make any material changes to this privacy policy (changes in the scope of personal data collected and the purpose of use), we will notify you through appropriate channels and obtain your consent.

### 7. How we uses cookies

To ensure our Website works correctly, we may at times place a small piece of data known as a cookie on your computer or mobile device. A cookie is a text file stored by a web server on a computer or mobile device. The content of a cookie can be retrieved or read only by the server that creates the cookie. The text in a cookie often consists of identifiers, site names, and some numbers and characters. Cookies are unique to the browsers or mobile applications you use, and enable websites to store data such as your preferences.

Like many other websites or Internet service providers, we use cookies to improve user experience. To be specific, the purposes are as follows:

*  Cookies allow websites to remember your settings such as language, font size on your computer or mobile device, or other browser preferences. This means that a user does not need to reset preferences for every visit.
*  Authentication. When you visit the Website, we may create a unique ID to identify you. If cookies with this function are not used, the Website will treat you as a new visitor every time you load a web page. For example, if you are redirected to another web page from the Website you are already logged in to and then return to the Website, it will not recognize you and you must log in again.
*  Statistics and analysis. For instance, we use cookie to count the number of visitors of the Website and to learn about sources of your visiting.
*  Security. We use cookie to secure the operation of the Website.

You can manage or delete cookies based on your own preferences. For details, visit AboutCookies.org (https://www.aboutcookies.org/). You can clear all the cookies stored on your computer, and most web browsers provide the option of blocking cookies. However, by doing so, you have to change the user settings every time you visit our Website.


### 8. How to contact us

If you have further questions or requests, please contact us by:

**[contact@openlookeng.io](mailto:contact@openlookeng.io)**

