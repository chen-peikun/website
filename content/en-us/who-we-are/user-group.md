+++
title = "User Group"
weight = 10
small_title = "User Group Members"

[[item]]
    title = "Michael Li"
    avatar = "img/foundation/MichaelLi.png"
    range = "Team lead for Engine, Performance, Curation teams in Analytics Platform Lab"
    content = "Michael leads and guides engineers and researchers on multiple R&D projects. Nearly 2 years at Huawei Canada with Data Engine team, lead design, architecture, and delivery of key benchmarks. With 20 years of core experience with IBM and Platform Computing."

[[item]]
    title = "Raghunandan"
    avatar = "img/foundation/Raghunandan.png"
    range = "Senior member of Database Engine R&D team in Analytics Platform Lab"
    content = "Raghunandan leads the BigData platform system Engineering Group at BRC, which focuses on adding performance, usability, maintability features to the available Opensources big data components. Under his technical leadership, team has built competitive features like Ultra Large mono clusters (20K nodes) and Zero downtime solution for Intra City BigData HA cluster deployments."

[[item]]
    title = "Denghong Liao"
    avatar = "img/foundation/DenghongLiao.png"
    range = "Huawei Engineer"
    content = "Denghong has participated in the openLooKeng project’s needs analysis, code development and project POC verification, etc., is committed to the optimization of cross-source and cross-domain high-performance query scenarios in the filed of big data, and has an in-depth understanding of openLooKeng’s solutions and industry applications."

[[item]]
    title = "Dezhi Xu"
    avatar = "img/foundation/DezhiXu.png"
    range = "Huawei R&D Engineer"
    content = "Dezhi has been engaged in the demand anlysis, development and tesing of products and overall solutions in the filed of Huawei storage and big data for more than 10 years."

[[item]]
    title = "Yan Huang"
    avatar = "img/foundation/YanHuang.png"
    range = "Huawei Big Data Technology Expert"
    content = "Yan has been focused on areas including big data metadata management, data lineage, and data virtualization technology. He is currently responsible for clarification of customer needs and solution design of the openLooKeng project, and leading the team in charge of partner development and customer support in openLooKeng project."






+++


## User Group 


The User Group helps end users on how to use openLooKeng, and collects improvemeng suggestions to the PMC. The User Group provides mail list and Slack for downstream users to communicate and provide general questions and discussion on using openLooKeng.

Mail List: users@openlookeng.io

