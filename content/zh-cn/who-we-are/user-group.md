+++
title = "用户支持组"
weight = 10
small_title = "用户组成员"

[[item]]
    title = "Michael Li"
    avatar = "img/foundation/MichaelLi.png"
    range = "Team lead for Engine, Performance, Curation teams in Analytics Platform Lab"
    content = "Michael 在华为加拿大研究所工作，在数据引擎团队工作了近2年，负责设计、架构和关键特性的开发，并拥有20年的IBM和Platform Computing工作经验。"


[[item]]
    title = "Raghunandan"
    avatar = "img/foundation/Raghunandan.png"
    range = "Chief Architect at Bangalore Research Centre, Committer of Apache CarbonData project"
    content = "Raghunandan是华为班加罗尔研究所大数据平台系统工程小组技术负责人，致力于大数据组件的性能优化、可用性、可维护性等功能的设计和开发。在他的领导下，团队在Intra City BigData HA Cluster项目中构筑了多个关键技术竞争力，包括超大集群（2万节点）部署和零宕机解决方案等。"

[[item]]
    title = "廖登宏"
    avatar = "img/foundation/DenghongLiao.png"
    range = "Huawei Engineer"
    content = "廖登宏长期参与openLooKeng项目的需求分析、代码开发与项目POC验证等工作，致力于大数据领域跨源跨域的高性能查询场景的优化，对openLooKeng的解决方案及其行业应用有深入理解。"

[[item]]
    title = "许德智"
    avatar = "img/foundation/DezhiXu.png"
    range = "Huawei R&D Engineer"
    content = "许德智从事华为存储和大数据领域的产品和整体解决方案的需求分析、开发、测试工作十余年。"

[[item]]
    title = "黄焰"
    avatar = "img/foundation/YanHuang.png"
    range = "Huawei Big Data Technology Expert"
    content = "黄焰专注的领域包含大数据元数据管理、数据血缘以及数据虚拟化技术。目前负责openLooKeng项目客户需求澄清及解决方案设计，同时带领团队负责openLooKeng开源项目伙伴拓展及客户支持工作。"






+++


## 用户支持组

用户组可帮助最终用户了解如何使用openLooKeng，并向PMC收集改进建议。用户组提供邮件列表和Slack，供下游用户进行交流，并提供有关使用openLooKeng的一般问题和讨论。


邮件列表：users@openlookeng.io

